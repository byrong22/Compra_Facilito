﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Proyecto.Models;
using Microsoft.AspNet.Identity;
using System.Web.UI.WebControls;
using System.IO;
using PagedList;

namespace Proyecto.Controllers
{

    public class bebidasController : Controller
    {
        private bproyecto db = new bproyecto();

        // GET: bebidas
         public ActionResult Index( int page=1)
          {
              return View(db.bebidas.OrderByDescending(v => v.idbebida).ToPagedList(page,8));
          }

        

        public ActionResult buscarbebida(string palabra)
        {

            if (!String.IsNullOrEmpty(palabra))
            {
                return View(db.bebidas.Where(x => x.nombre.Contains(palabra)).ToList());
            }
            return View(db.bebidas.ToList());
        }

        public ActionResult misbebidas()
        {
            return View(db.bebidas.ToList());
        }

        // GET: bebidas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            bebidas bebidas = db.bebidas.Find(id);
            if (bebidas == null)
            {
                return HttpNotFound();
            }
            return View(bebidas);
        }

        // GET: bebidas/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }
        [Authorize]
        // POST: bebidas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idbebida,nombre,usuario,idusuario,foto,ingredientes,preparacion")] bebidas bebidas)
        {
            if (ModelState.IsValid)
            {
                bebidas.usuario=User.Identity.GetUserName();
                db.bebidas.Add(bebidas);
                db.SaveChanges();
                return RedirectToAction("Index");
                
            } 
            return View(bebidas);
        }
        


        // GET: bebidas/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            bebidas bebidas = db.bebidas.Find(id);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            if (bebidas == null)
            {
                return HttpNotFound();
            }
            return View(bebidas);
        }

        // POST: bebidas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idbebida,nombre,usuario,idusuario,foto,ingredientes,preparacion")] bebidas bebidas)
        { 
            if (ModelState.IsValid)
            {
                db.Entry(bebidas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bebidas);
        }


        private const string direccion= "~/Fotos/";
         

        // GET: bebidas/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            bebidas bebidas = db.bebidas.Find(id);
            if (bebidas == null)
            {
                return HttpNotFound();
            }
            return View(bebidas);
        }

        // POST: bebidas/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            bebidas bebidas = db.bebidas.Find(id);
            db.bebidas.Remove(bebidas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult FileUploadUpdate(HttpPostedFileBase file)
        {
            int id = Convert.ToInt16(Request.Form["idbebida"]);
            bproyecto db = new bproyecto();

            //Consulta para retomar y cambiar informacion de artista
            var query = from ord in db.bebidas where ord.idbebida == id select ord;
            foreach (bebidas ord in query)
            {
                ord.nombre = Request.Form["nombre"]; 
                ord.ingredientes = Request.Form["ingredientes"];
                ord.preparacion = Request.Form["preparacion"];
                ord.idbebida = Convert.ToInt16(Request.Form["idbebida"]);
                ord.idusuario = Request.Form["idusuario"];
                string oldFoto = ord.foto;

                if (file != null)
                {
                    string ImageName = System.IO.Path.GetFileName(file.FileName);
                    ord.foto = ImageName;
                    //Eliminar foto
                    if (System.IO.File.Exists(Server.MapPath("~/Fotos/" + oldFoto)))
                    {
                        System.IO.File.Delete(oldFoto);
                    }
                    string physicalPath = Server.MapPath("~/Fotos/" + ImageName);
                    // save image in folder
                    file.SaveAs(physicalPath);

                }
            }
            try
            {
                //save new record in database
                db.SaveChanges();
            }
            catch (Exception e)
            {
            }

            //Display records
            return RedirectToAction("Index");
        }

 

        //subir imagen en el Create
        public ActionResult FileUpload(HttpPostedFileBase file)
        {

            if (file != null)
            {
                bproyecto db = new bproyecto();
                string ImageName = System.IO.Path.GetFileName(file.FileName);
                string physicalPath = Server.MapPath("~/Fotos/" + ImageName);

                // save image in folder
                file.SaveAs(physicalPath);

                //save new record in database
                bebidas newRecord = new  bebidas();
                newRecord.nombre = Request.Form["nombre"];
                newRecord.ingredientes = Request.Form["ingredientes"];
                newRecord.preparacion = Request.Form["preparacion"];
                newRecord.idbebida = Convert.ToInt16(Request.Form["idbebida"]);
                newRecord.idusuario = Request.Form["idusuario"];
                newRecord.usuario = User.Identity.GetUserName();
                newRecord.foto = ImageName;
                db.bebidas.Add(newRecord);
                db.SaveChanges();

            } 
            //Display records
            return RedirectToAction("misbebidas");
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
