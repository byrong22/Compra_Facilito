namespace Proyecto.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class bproyecto : DbContext
    {
        public bproyecto()
            : base("name=bproyecto")
        {
        }

        public virtual DbSet<bebidas> bebidas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<bebidas>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<bebidas>()
                .Property(e => e.foto)
                .IsUnicode(false);

            modelBuilder.Entity<bebidas>()
                .Property(e => e.ingredientes)
                .IsUnicode(false);

            modelBuilder.Entity<bebidas>()
                .Property(e => e.preparacion)
                .IsUnicode(false);
        }
         
    }
}
