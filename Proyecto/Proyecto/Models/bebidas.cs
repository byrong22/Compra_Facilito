using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
namespace Proyecto.Models
{
    public partial class bebidas
    {
        [Key]
        public int idbebida { get; set; }
        [Required(ErrorMessage = "Completa este campo")]

        [StringLength(100)]
        public string nombre { get; set; }

        [StringLength(255)]
        public string foto { get; set; }

        [Required(ErrorMessage = "Completa este campo")]
        [DataType(DataType.MultilineText)]
        public string ingredientes { get; set; }

        [Required(ErrorMessage = "Completa este campo")]
        [DataType(DataType.MultilineText)] 
        public string preparacion { get; set; }

        [StringLength(128)]
        public string idusuario { get; set; }

        
        [StringLength(256)]
        public string usuario { get; set; }  
    }
}
